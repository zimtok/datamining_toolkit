from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from tkinter import ttk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

class PlotView:

    def __init__(self, root, dataset):
        self.root = root
        self.dataset = dataset
        self.option_frame = ttk.Frame(self.root)
        self.option_frame.grid(row=0, column=0)
        self.data_frame = ttk.Frame(self.root)
        self.data_frame.grid(row=0, column=1)
        self.row_index = 0
        self.save_button = None
        if self.dataset is None:
            messagebox.showerror(title="Error", message="You haven't loaded any data")
        else:
            self.list_box = None
            self.labels = None
            self.figure = None
            self.create_option_frame()

    def create_option_frame(self):
        ttk.Label(self.option_frame, text="Select the columns to use").grid(row=self.row_index, column=0)
        self.row_index += 1
        self.labels = self.create_label_list()
        self.list_box = Listbox(self.option_frame, selectmode=MULTIPLE, height=len(self.labels))
        self.list_box.grid(row=self.row_index, column=0)
        self.row_index += 1
        ttk.Button(self.option_frame,text="Create plot", command=self.update_figure).grid(row=self.row_index, column=0)
        self.row_index += 1

        for i in range(0, len(self.labels)):
            self.list_box.insert(i, self.labels[i])

        for child in self.option_frame.winfo_children():

            child.grid_configure(padx=10, pady=10)


    def get_data(self):
        return self.dataset

    def create_label_list(self):
        ret = list()
        for label in self.dataset.get_column_labels():
            ret.append(label)
        return ret

    def update_figure(self):
        self.figure = self.get_figure(self.get_selected_columns())
        canvas = FigureCanvasTkAgg(self.figure, master=self.data_frame)
        canvas.get_tk_widget().grid(row=0, column=0)
        canvas.draw()
        if self.save_button is None:
            self.save_button = ttk.Button(self.option_frame, text="Save plot", command = self.save)
        self.save_button.grid(row=self.row_index, column=0)
        self.row_index += 1
        for child in self.data_frame.winfo_children():
            child.grid_configure(padx=10, pady=10)

    def get_selected_columns(self):
        ret = list()
        selection = self.list_box.curselection()
        if len(selection) == 0:
            messagebox.showerror(title="Error", message="You haven't selected any columns")
        for index in selection:
            label = self.labels[index]
            ret.append(self.dataset.get_column(label))
        return ret

    def get_figure(self, columns):
        pass

    def destroy(self):
        self.option_frame.destroy()
        self.data_frame.destroy()

    def save(self):
        outfile_name = filedialog.asksaveasfilename()
        if outfile_name != '':
            self.figure.savefig(outfile_name)
