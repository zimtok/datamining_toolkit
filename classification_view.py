from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from tkinter import ttk

import numpy as np

import algorithm_view as aw
import algorithms.dataset as ds
import algorithms.k_nearest_neighbours as knn
import algorithms.naive_bayes as nb
import visualisation.scatterplot as sp
from algorithms.algorithm_exception import AlgorithmException


class ClassificationView(aw.AlgorithmView):
    """
    View class for classification algorithms
    """

    methods = {"k-Nearest Neighbours": knn.KNearestNeighbours, "Naive Bayes Classifier": nb.NaiveBayes}

    def __init__(self, root, dataset):
        self.classes = StringVar()
        self.classification_alg = None
        self.result_dataset = None
        aw.AlgorithmView.__init__(self, root, dataset)



    def create_option_frame(self):
        """
        Create the option frame with an extra combo box for the class values
        :return: None
        """
        self.add_method_selector()
        ttk.Label(self.option_frame, text="Column containg class values").grid(row=self.row_index, column=0)
        ttk.Combobox(self.option_frame, state="readonly", values=[x for x in self.dataset.get_column_labels()], textvariable=self.classes).grid(row=self.row_index, column=1)
        self.row_index += 1
        ttk.Button(self.option_frame, text='Train model', command=self.run_method).grid(row=self.row_index, column=0)
        self.row_index += 1
        ttk.Button(self.option_frame, text='Make prediction', command=self.run_prediction).grid(row=self.row_index, column=0)
        self.row_index += 1


    def run_method(self):
        """
        Train the model on the provided data
        :return: None
        """

        if len(self.method.get()) > 0 and len(self.classes.get()) > 0:
            try:
                class_vector, data_matrix = self.dataset.extract_column(self.classes.get())
                selected_method = self.methods[self.method.get()]
                self.classification_alg = selected_method()
                self.classification_alg.set_data(data_matrix)
                self.classification_alg.set_classes(class_vector)
                self.classification_alg.set_parameters(self.create_parameter_dict())
                self.classification_alg.train()
            except AlgorithmException as exception:
                messagebox.showerror(title="Error", message=exception.message)


    def run_prediction(self):
        """
        Predict on each row of a different dataset read from a file
        :return:
        """
        filename = filedialog.askopenfilename()
        if len(filename) > 0:
            try:
                prediction_data = ds.DataSet()
                prediction_data.read_from_file(filename)
                self.add_prediction_to_dataset(prediction_data)
                self.result_dataset = prediction_data
                self.clear_result_buttons()
                self.add_result_buttons()
            except AlgorithmException as exception:
                messagebox.showerror(title="Error", message=exception.message)




    def set_up_data_frame(self, dataset):
        """
        Set up data_frame with class values
        :param dataset: algorithms.DataSet
        :return: None
        """
        for i in range(0, dataset.get_dimension()):
            label = dataset.get_header(i)
            ttk.Label(self.data_frame, text=label).grid(row=0, column=i)

    def add_prediction_to_dataset(self, dataset):
        """
        Add the predicted values to a dataset
        :param dataset: algorithms.DataSet
        :return: None
        """
        try:
            prediction_result = np.matrix(np.zeros((dataset.get_size(), 1)))
            for i in range(0, dataset.get_size()):
                row = dataset.get_row(i)
                class_value = self.classification_alg.predict(row)
                prediction_result[i, 0] = class_value
            new_label = self.generate_unique_label(dataset, "predicted_class_{}")
            dataset.append_column(prediction_result, new_label)
        except AttributeError:
           raise  AlgorithmException("You haven't trained your model yet")


    def add_to_data_frame(self, dataset):
        """
        Display a dataset in the data_rame
        :param dataset: algorithms.DataSet
        :return:
        """
        for i in range(0, dataset.get_size()):
            row = dataset.get_row(i)
            for j in range(0, row.shape[1]):
                ttk.Label(self.data_frame, text=row[0,j]).grid(row=i+1, column=j)

    def update_plot(self):
        """
        Create a scatter plot with colorised according to class values
        :return: None
        """
        class_header = self.result_dataset.get_header(self.result_dataset.get_dimension()-1)
        groups, data_matrix = self.result_dataset.extract_column(class_header)
        scatter_plot = sp.ScatterPlot(data_matrix, groups=groups)
        scatter_plot.create_plot()
        self.figure = scatter_plot.get_figure()


    def get_data(self):
        """
        Return result of the prediction
        :return: algorithms.DataSet
        """
        if not self.result_dataset is None:
            return self.result_dataset
        else:
            return self.dataset
