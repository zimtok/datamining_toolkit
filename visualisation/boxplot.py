from matplotlib.figure import Figure
import colorsys
import numpy as np

class BoxPlot:
    fig_height = 5
    fig_width = 5

    def __init__(self, columns):
        self.data = list()
        for col in columns:
            self.data.append(col.getA1())
        self.fig = Figure(figsize=(self.fig_width, self.fig_height))
        self.plot = None

    def create_plot(self):
        self.plot = self.fig.add_subplot(111)
        colors = self.generate_colors(len(self.data))
        for i in range(0,len(self.data)):
            self.plot.boxplot(self.data)


    def generate_colors(self, colors):
        HSV_tuples = [(x * 1.0 / colors, 0.8, 0.8) for x in range(colors)]
        return list(map(lambda x: colorsys.hsv_to_rgb(*x), HSV_tuples))

    def get_figure(self):
        return self.fig