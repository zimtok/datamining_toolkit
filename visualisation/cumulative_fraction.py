from matplotlib.figure import Figure
import colorsys
import numpy as np

class CumulativeFraction:
    fig_height = 5
    fig_width = 5

    def __init__(self, columns):
        self.columns = columns
        self.fig = Figure(figsize=(self.fig_width, self.fig_height))
        self.plot = None


    def create_plot(self):
        self.plot = self.fig.add_subplot(111)
        colors = self.generate_colors(len(self.columns))
        for i in range(0,len(self.columns)):
            self.plot.plot(*self.get_graph_points(self.columns[i]), color=colors[i])

    def get_graph_points(self, column):
        sorted_data = np.sort(column.getA1())
        xvalues = [0]
        yvalues = [0]
        for i in range(0, len(sorted_data)):
            xvalues.append(sorted_data[i])
            xvalues.append(sorted_data[i])
            yvalues.append(i/len(sorted_data))
            yvalues.append((i+1)/len(sorted_data))
        return (xvalues, yvalues)

    def generate_colors(self, colors):
        HSV_tuples = [(x * 1.0 / colors, 0.8, 0.8) for x in range(colors)]
        return list(map(lambda x: colorsys.hsv_to_rgb(*x), HSV_tuples))

    def get_figure(self):
        return self.fig