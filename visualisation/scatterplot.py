from matplotlib.figure import Figure
from mpl_toolkits.mplot3d import Axes3D
import warnings
import colorsys

class ScatterPlot:

    fig_height = 6
    fig_width = 6

    def __init__(self, data, groups=None, colors=None):
        #groups[i] is the group to which the i-th row of data belongs
        #colors[j] is the color for the j-th group
        self.data = data
        self.groups = groups
        self.colors = colors
        self.fig = Figure(figsize=(self.fig_width, self.fig_height))
        self.plot = None


    def create_plot(self):
        if self.data.shape[1] == 1:
            self.create_1d_plot()
        elif self.data.shape[1] == 2:
            self.create_2d_plot()
        elif self.data.shape[1] == 3:
            self.create_3d_plot()
        else:
            self.fig = None

    def create_1d_plot(self):
        self.plot = self.fig.add_subplot(111)
        if not self.groups is None:
            if self.colors is None:
                self.colors = self.generate_colors(len(set(self.groups)))
            for i in range(0, len(self.colors)):
                color = self.colors[i]
                x_coords = [self.data[j, 0] for j in range(0, len(self.groups)) if self.groups[j] == i]
                y_coords = [0 for j in range(0, len(self.groups)) if self.groups[j] == i]
                self.plot.plot(x_coords, y_coords, 'o', color=color)
        else:
            x_coords = [self.data[i, 0] for i in range(0, self.data.shape[0])]
            y_coords = [0 for i in range(0, self.data.shape[0])]
            self.plot.plot(x_coords, y_coords, 'o')


    def create_2d_plot(self):
        self.plot = self.fig.add_subplot(111)
        if not self.groups is None:
            if self.colors is None:
                self.colors = self.generate_colors(len(set(self.groups)))
            for i in range(0, len(self.colors)):
                color = self.colors[i]
                x_coords = [self.data[j, 0] for j in range(0,len(self.groups)) if self.groups[j] == i]
                y_coords = [self.data[j, 1] for j in range(0,len(self.groups)) if self.groups[j] == i]
                self.plot.plot(x_coords, y_coords, 'o', color=color)
        else:
            x_coords = [self.data[i, 0] for i in range(0, self.data.shape[0])]
            y_coords = [self.data[i, 1] for i in range(0, self.data.shape[0])]
            self.plot.plot(x_coords, y_coords, 'o')

    def create_3d_plot(self):
        with warnings.catch_warnings():
            #unfortunately, matplotlib uses deprecated calls here, so we suppress the warnings
            warnings.simplefilter("ignore")
            warnings.simplefilter(action='ignore', category=FutureWarning)
            self.plot = self.fig.add_subplot(111, projection='3d')
            if not self.groups is None:
                if self.colors is None:
                    self.colors = self.generate_colors(len(set(self.groups)))
                for i in range(0, len(self.colors)):
                    color = self.colors[i]
                    x_coords = [self.data[j, 0] for j in range(0,len(self.groups)) if self.groups[j] == i]
                    y_coords = [self.data[j, 1] for j in range(0,len(self.groups)) if self.groups[j] == i]
                    z_coords = [self.data[j, 2] for j in range(0,len(self.groups)) if self.groups[j] == i]
                    self.plot.scatter(xs=x_coords, ys=y_coords, zs=z_coords, marker='o', color=color)
            else:
                x_coords = [self.data[i, 0] for i in range(0, self.data.shape[0])]
                y_coords = [self.data[i, 1] for i in range(0, self.data.shape[0])]
                z_coords = [self.data[i, 2] for i in range(0, self.data.shape[0])]
                self.plot.scatter(xs=x_coords, ys=y_coords, zs=z_coords, marker='o')


    def add_line(self, line_x, line_y, line_z):
        if len(line_z) > 0 :
            self.plot.plot(xs=line_x, ys=line_z, zs=line_y)
        else:
            self.plot.plot(line_x, line_y)

    def show_plot(self):
        self.fig.show()

    def get_figure(self):
        return self.fig

    def generate_colors(self, colors):
        HSV_tuples = [(x * 1.0 / colors, 0.9, 0.9) for x in range(colors)]
        return list(map(lambda x: colorsys.hsv_to_rgb(*x), HSV_tuples))


