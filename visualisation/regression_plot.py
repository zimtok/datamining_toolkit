import  visualisation.scatterplot as sp


class RegressionPlot:

    def __init__(self, data, reg):
        self.data = data
        self.reg = reg
        self.plot = None

    def create_plot(self, min=0, max=0):
        point_data = self.reg.get_point_data()
        line_x = []
        line_y = []
        line_z = []
        line_x.append(self.reg.explanatory[min, 1])
        line_x.append(self.reg.explanatory[max, 1])
        line_y.append(self.reg.predict(self.reg.explanatory[min, 1:])[0, 0])
        line_y.append(self.reg.predict(self.reg.explanatory[max, 1:])[0, 0])
        if self.reg.explanatory.shape[1] > 2:
            line_z.append(self.reg.explanatory[min, 2])
            line_z.append(self.reg.explanatory[max, 2])

        self.plot = sp.ScatterPlot(point_data)


        self.plot.create_plot()
        self.plot.add_line(line_x, line_y, line_z)

    def get_figure(self):
        return self.plot.get_figure()