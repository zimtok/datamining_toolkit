import numpy as np
from unittest import TestCase
from .. import dataset as ds



class TestDataSet(TestCase):
    def setUp(self):
        data_matrix = np.matrix([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
        self.dataset = ds.DataSet()
        self.dataset.create_from_matrix(data_matrix)
        for i in range(0, 3):
            self.dataset.set_header("header_{}".format(i), i)


    def test_clone(self):
        ds2 = self.dataset.clone()
        self.assertTrue(np.array_equal(self.dataset.data, ds2.data))

    def test_get_column(self):
        col = self.dataset.get_column(0)
        self.assertTrue(np.array_equal(col, np.matrix([[1], [4], [7]])))
        col = self.dataset.get_column(1)
        self.assertTrue(np.array_equal(col, np.matrix([[2], [5], [8]])))
        col = self.dataset.get_column(2)
        self.assertTrue(np.array_equal(col, np.matrix([[3], [6], [9]])))


        with self.assertRaises(KeyError):
            col = self.dataset.get_column(5)

    def test_get_column_number(self):
        num = self.dataset.get_column_number("header_2")
        self.assertEqual(num, 2)
        num = self.dataset.get_column_number("asd")
        self.assertIsNone(num)

    def test_get_header(self):
        header = self.dataset.get_header(0)
        self.assertEquals(header, "header_0")
        header = self.dataset.get_header(42)
        self.assertIsNone(header)

    def test_get_row(self):
        row = self.dataset.get_row(0)
        self.assertTrue(np.array_equal(row, np.matrix([1, 2, 3])))
        row = self.dataset.get_row(1)
        self.assertTrue(np.array_equal(row, np.matrix([4, 5, 6])))
        row = self.dataset.get_row(2)
        self.assertTrue(np.array_equal(row, np.matrix([7, 8, 9])))


    def test_get_rows(self):
        expected = [np.matrix([1,2,3]), np.matrix([4,5,6]), np.matrix([7,8,9])]
        result = []
        for row in self.dataset.get_rows():
            result.append(row)

        self.assertEquals(len(result), 3)
        for i in range(0, 3):
            self.assertTrue(np.array_equal(result[i], expected[i]))


    def test_get_column_labels(self):
        result = set()
        for label in self.dataset.get_column_labels():
            result.add(label)

        self.assertEquals(len(result), 3)
        self.assertTrue('header_0' in result)
        self.assertTrue('header_1' in result)
        self.assertTrue('header_2' in result)

    def test_get_row_dict(self):
        expected =[
            {'header_0': 1, 'header_1': 2, 'header_2': 3},
            {'header_0': 4, 'header_1': 5, 'header_2': 6},
            {'header_0': 7, 'header_1': 8, 'header_2': 9},
        ]
        for i in range(0,3):
            self.assertEquals(self.dataset.get_row_dict(i), expected[i])

    def test_get_dimension(self):
        self.assertEquals(self.dataset.get_dimension(), 3)

    def test_get_size(self):
        self.assertEquals(self.dataset.get_size(), 3)

    def test_get_data_matrix(self):
        data_matrix = self.dataset.get_data_matrix()
        self.assertTrue(np.array_equal(data_matrix, np.matrix([[1, 2, 3], [4, 5, 6], [7, 8, 9]])))

    def test_append_row(self):
        self.dataset.append_row(np.matrix([10, 11, 12]))
        self.assertEquals(self.dataset.get_size(), 4)
        last_row = self.dataset.get_row(self.dataset.get_size() -1)
        self.assertTrue(np.array_equal(last_row, np.matrix([10, 11, 12])))

    def test_append_row_list(self):
        self.dataset.append_row([10, 11, 12])
        self.assertEquals(self.dataset.get_size(), 4)
        last_row = self.dataset.get_row(self.dataset.get_size() - 1)
        self.assertTrue(np.array_equal(last_row, np.matrix([10, 11, 12])))

    def test_append_row_array(self):
        self.dataset.append_row(np.array([10, 11, 12]))
        self.assertEquals(self.dataset.get_size(), 4)
        last_row = self.dataset.get_row(self.dataset.get_size() - 1)
        self.assertTrue(np.array_equal(last_row, np.matrix([10, 11, 12])))

    def test_append_column(self):
        self.dataset.append_column(np.matrix([[-1], [-2], [-3]]))
        self.assertEquals(self.dataset.get_dimension(), 4)
        last_col = self.dataset.get_column(self.dataset.get_dimension() - 1)
        self.assertTrue(np.array_equal(last_col, np.matrix([[-1], [-2], [-3]])))

    def test_append_column_with_header(self):
        self.dataset.append_column(np.matrix([[-1], [-2], [-3]]), "header_3")
        self.assertEquals(self.dataset.get_dimension(), 4)
        last_col = self.dataset.get_column("header_3")
        self.assertTrue(np.array_equal(last_col, np.matrix([[-1], [-2], [-3]])))
        with self.assertRaises(KeyError):
            self.dataset.append_column(np.matrix([[-1], [-2], [-3]]), "header_3")

    def test_append_column_list(self):
        self.dataset.append_column([-1, -2, -3])
        self.assertEquals(self.dataset.get_dimension(), 4)
        last_col = self.dataset.get_column(self.dataset.get_dimension() - 1)
        self.assertTrue(np.array_equal(last_col, np.matrix([[-1], [-2], [-3]])))

    def test_append_column_nested_list(self):
        self.dataset.append_column([[-1], [-2], [-3]])
        self.assertEquals(self.dataset.get_dimension(), 4)
        last_col = self.dataset.get_column(self.dataset.get_dimension() - 1)
        self.assertTrue(np.array_equal(last_col, np.matrix([[-1], [-2], [-3]])))

    def test_append_column_array(self):
        self.dataset.append_column(np.array([-1, -2, -3]))
        self.assertEquals(self.dataset.get_dimension(), 4)
        last_col = self.dataset.get_column(self.dataset.get_dimension() - 1)
        self.assertTrue(np.array_equal(last_col, np.matrix([[-1], [-2], [-3]])))

    def test_extract_column(self):
        col, matrix = self.dataset.extract_column("header_0")
        self.assertTrue(np.array_equal(col, np.array([1, 4, 7])))
        self.assertTrue(np.array_equal(matrix, np.matrix([[2, 3], [5, 6], [8, 9]])))
        col, matrix = self.dataset.extract_column("header_1")
        self.assertTrue(np.array_equal(col, np.array([2, 5, 8])))
        self.assertTrue(np.array_equal(matrix, np.matrix([[1, 3], [4, 6], [7, 9]])))
        col, matrix = self.dataset.extract_column("header_2")
        self.assertTrue(np.array_equal(col, np.array([3, 6, 9])))
        self.assertTrue(np.array_equal(matrix, np.matrix([[1, 2], [4, 5], [7, 8]])))



    def test_has_header(self):
        for i in range(0,3):
            self.assertTrue(self.dataset.has_header("header_{}".format(i)))
        self.assertFalse(self.dataset.has_header("asd"))
        self.assertFalse(self.dataset.has_header(""))
        self.assertFalse(self.dataset.has_header(None))
        self.assertFalse(self.dataset.has_header(1))

    def test_set_header(self):
        self.dataset.set_header("new_header", 2)
        self.assertTrue("new_header" in self.dataset.headers)
        self.assertFalse("header_2" in self.dataset.headers)
