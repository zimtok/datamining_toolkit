from unittest import TestCase
from unittest.mock import Mock
import numpy as np
from .. import dataset as ds
from .. import descriptive_stat as stat


class TestDescriptiveStatistics(TestCase):

    def setUp(self):
        matrix = np.matrix([[1, 2, 0, -1, 0.2], [2, 2, 0, 0, 0.01], [3, 2, 0, 5, 0.8], [4, 2, 0, -6, 1]])
        data = ds.DataSet()
        data.create_from_matrix(matrix)
        data.set_header("natural", 0)
        data.set_header("identical", 1)
        data.set_header("zeros", 2)
        data.set_header("negative", 3)
        data.set_header("fraction", 4)
        self.stat = stat.DescriptiveStatistics(data)
        short_matrix = np.matrix([1])
        short_data = ds.DataSet()
        short_data.create_from_matrix(short_matrix)
        short_data.set_header("short", 0)
        self.short_stat = stat.DescriptiveStatistics(short_data)



    def test_min(self):
        self.assertEquals(self.stat.min("natural"), 1)
        self.assertEquals(self.stat.min("identical"), 2)
        self.assertEquals(self.stat.min("zeros"), 0)
        self.assertEquals(self.stat.min("negative"), -6)
        self.assertEquals(self.stat.min("fraction"), 0.01)
        self.assertEquals(self.short_stat.min("short"), 1)

    def test_max(self):
        self.assertEquals(self.stat.max("natural"), 4)
        self.assertEquals(self.stat.max("identical"), 2)
        self.assertEquals(self.stat.max("zeros"), 0)
        self.assertEquals(self.stat.max("negative"), 5)
        self.assertEquals(self.stat.max("fraction"), 1)
        self.assertEquals(self.short_stat.max("short"), 1)

    def test_mean(self):
        self.assertEquals(self.stat.mean("natural"), 2.5)
        self.assertEquals(self.stat.mean("identical"), 2)
        self.assertEquals(self.stat.mean("zeros"), 0)
        self.assertEquals(self.stat.mean("negative"), -0.5)
        self.assertAlmostEqual(self.stat.mean("fraction"), 0.5025)
        self.assertEquals(self.short_stat.mean("short"), 1)

    def test_median(self):
        self.assertEquals(self.stat.median("natural"), 2.5)
        self.assertEquals(self.stat.median("identical"), 2)
        self.assertEquals(self.stat.median("zeros"), 0)
        self.assertEquals(self.stat.median("negative"), -0.5)
        self.assertEquals(self.stat.median("fraction"), 0.5)
        self.assertEquals(self.short_stat.median("short"), 1)

    def test_std_dev(self):
        self.assertAlmostEquals(self.stat.std_dev("natural"), 1.2909944487358)
        self.assertEquals(self.stat.std_dev("identical"), 0)
        self.assertEquals(self.stat.std_dev("zeros"), 0)
        self.assertAlmostEquals(self.stat.std_dev("negative"), 4.5092497528229)
        self.assertAlmostEqual(self.stat.std_dev("fraction"), 0.47260801234568)
        self.assertEquals(self.short_stat.std_dev("short"), 0)

    def test_get_all_stat(self):
        labels = ["natural", "identical", "zeros", "negative", "fraction"]
        row_count = 0
        for row in self.stat.get_all_stat():
            self.assertTrue(row[0] in labels)
            self.assertEquals(len(row), 6)
            row_count += 1
        self.assertEquals(row_count, 5)


