import numpy as np
import algorithms.abstract_algorithm as alg
from algorithms.algorithm_exception import AlgorithmException


class NaiveBayes(alg.AbstractAlgorithm):
    """
    Implementation of the Naive Bayes Classifier using a Gaussian probability density function
    """

    def __init__(self):
        alg.AbstractAlgorithm.__init__(self)
        self.classes = None
        self.class_data = None

    def set_classes(self, classes):
        """
        Set the vector containing the class vlaues
        :param classes: numpy.array
        :return: None
        """
        self.classes = classes

    def train(self):
        """
        Train the model on the current data
        :return: None
        """
        self.init_class_data(self.split_data())

    def predict(self, row):
        """
        Return the class value to which the specified point belongs to with the highest probability
        :param row: numpy.matrix
        :return: int
        """
        if row.shape[1] != self.data.shape[1]:
            raise AlgorithmException("The data for prediction must have the same number of variables as the dependent variables")
        max_class = None
        max_probablity = 0
        for key in self.class_data:
            current_class = self.class_data[key]
            probability = current_class["fraction"]
            for i in range(0, self.data.shape[1]):
                probability *= self.get_probability_density(row[0, i], current_class["means"][0, i], current_class["variances"][0,i])
            if probability > max_probablity:
                max_probablity = probability
                max_class = key
        return max_class


    def split_data(self):
        """
        Split the data matrix into more matrices according to the class values
        Return the result as a {class_value: sub_matrix} dict
        :return: dict
        """
        split = dict()
        for i in range(0, self.data.shape[0]):
            class_value = self.classes[i]
            if class_value in split:
                split[class_value] = np.concatenate((split[class_value], self.data[i, :]))
            else:
                split[class_value] = np.copy(self.data[i, :])

        return split

    def init_class_data(self, split_data):
        """
        Compute a priori probability, mean and variance for each class
        :param split_data: dict
        :return: None
        """
        self.class_data = dict()
        for key, matrix in split_data.items():
            self.class_data[key] = dict()
            self.class_data[key]["fraction"] = matrix.shape[0]/self.data.shape[0]
            self.class_data[key]["means"] = np.matrix(np.mean(matrix, axis=0, dtype=np.float64))
            self.class_data[key]["variances"] = np.matrix(np.var(matrix, axis=0, dtype=np.float64))

    def get_probability_density(self, x, mean, variance):
        """
        Gaussian probability density function
        :param x: float
        :param mean: float
        :param variance: float
        :return: float
        """
        exponent = (-(x - mean) ** 2) / (2 * variance)
        return 1 / np.sqrt(2 * np.pi * variance) * np.exp(exponent)














