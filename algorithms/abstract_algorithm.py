

class AbstractAlgorithm:
    """
    The base class for all algorithms implemented by the toolkit
    """


    def __init__(self):
        """
        Initialise the dictionary of setters
        """
        self.setters = dict()
        self.data = None

    def get_parameters(cls):
        """Return an alphanumerically sorted list of the parameter identifiers of the algorithm"""
        param_list = list(cls.setters.keys())
        param_list.sort()
        return param_list

    def set_parameters(self, new_parameters):
        """
        Set all parameter values supplied in as a parameter in {parameter_identifier: parameter_value} format
        Parameter values are expected to be floating point numbers: the individual setters make any necessary conversion
        :param new_parameters: dict
        :return: None
        """
        for param_name, param_value in new_parameters.items():
            setter = self.setters[param_name]
            setter(param_value)

    def run(self):
        """
        Run the algorithm with the currently set parameters
        :return: None
        """
        pass

    def set_data(self, data):
        """
        Set the matrix with the data that the algorithm will work on
        :param data: numpy.matrix
        :return: None
        """
        self.data = data


