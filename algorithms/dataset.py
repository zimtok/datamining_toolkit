import numpy as np
import csv
from algorithms.algorithm_exception import AlgorithmException


class DataSet:
    """
    Utility class for managing a matrix of data with labels for each column
    """
    def __init__(self):
        self.headers = dict()
        self.data = None

    def read_from_file(self, filename, header=True, sep=',', quote='"', convert_func=float):
        """
        Read a data set from a CSV file
        :param filename: str
        :param header: bool
        :param sep: str
        :param quote: str
        :param convert_func: function
        :return: None
        """
        try:
            with open(filename) as infile:
                reader = csv.reader(infile, delimiter=sep, quotechar=quote)
                if header:
                    header_list = next(reader)
                    for i in range(len(header_list)):
                        self.set_header(header_list[i], i)
                data_list = list()
                for row in reader:
                    data_list.append([convert_func(x) for x in row])
                for i in range(0, len(data_list)- 1):
                    if len(data_list[i]) != len(data_list[i+1]):
                        raise AlgorithmException("There are missing values in the file")
                self.data = np.matrix(data_list)
                del data_list
        except ValueError:
            raise AlgorithmException("The file contains non-numerical values")
        except FileNotFoundError:
            raise   AlgorithmException("File not found")

    def write_to_file(self, filename, header=True, sep=',', quote='"'):
        """
        Writes the data set to a CSV file
        :param filename: str
        :param header: bool
        :param sep: bool
        :param quote: str
        :return: None
        """
        with open(filename, 'w') as outfile:
            writer = csv.DictWriter(outfile, fieldnames=self.headers.keys())
            if header:
                writer.writeheader()
            for i in range(0,self.data.shape[0]):
                writer.writerow(self.get_row_dict(i))

    def create_from_matrix(self, matrix):
        """
        Copies an existing matrix to use as data
        :param matrix: numpy.matrix
        :return:
        """
        self.data = np.matrix(matrix)

    def clone(self):
        """
        Creates a deep copy of the dataset
        :return: DataSet
        """
        new_dataset = DataSet()
        new_dataset.create_from_matrix(self.data)
        for i in range(self.get_dimension()):
            new_dataset.set_header(self.get_header(i), i)
        return new_dataset

    def get_column(self, column):
        """
        Gets the specified column from the data set
        Raises KeyError if the column isn't found
        The column is returned as an (nx1) matrix
        :param column: int, str
        :return: numpy.matrix
        """
        if column in self.headers:
            col_number = self.headers[column]
        else:
            col_number = column
        try:
            return self.data[:, col_number]
        except IndexError:
            raise KeyError('The data set has no column named {}'.format(column))

    def get_column_number(self, header):
        """
        Gets the index of a column associated with a header
        Returns None if the header does no exist
        :param header: str
        :return: int
        """
        return self.headers.get(header)

    def get_header(self, col_num):
        """
        Gets the header for a given column
        :param col_num: int
        :return: str
        """
        for header in self.headers:
            if self.headers[header] == col_num:
                return header
        return None

    def get_row(self, num):
        """
        Get a single row from the data
        :param num: int
        :return: numpy.matrix
        """
        return self.data[num, :]

    def get_rows(self):
        """
        Creates a generator to iterate over all rows of the data
        :return: GeneratorType
        """
        for i in range(0, self.data.shape[0]):
            yield np.array(self.data[i, :])

    def get_column_labels(self):
        """
        Creates a generator to iterate over all column headers
        :return: GeneratorType
        """
        for label in self.headers:
            yield label

    def get_row_dict(self, row_num):
        """
        Returns a row in the form of a {header: value} dict
        :param row_num: int
        :return: dict
        """
        row_dict = dict()
        for header in self.headers:
            col_num = self.headers[header]
            row_dict[header] = self.data[row_num, col_num]
        return row_dict

    def get_dimension(self):
        """
        Get the number of columns in the data
        :return: int
        """
        return self.data.shape[1]

    def get_size(self):
        """
        Return the number of rows in the data
        :return: int
        """
        return self.data.shape[0]

    def get_data_matrix(self):
        """
        Return a view of the data as a matrix
        :return: numpy.matrix
        """
        return self.data

    def append_row(self, row):
        """
        Append a row to the end of the data
        :param row: numpy.matrix, numpy.array, list
        :return: None
        """
        row_array = np.asarray(row).reshape(1, self.data.shape[1])
        self.data = np.concatenate((self.data, row_array))

    def append_column(self, column, header=None):
        """
        Append a column after the rightmost column of the data with a specified header
        :param column: numpy.matrix, numpy.array, list
        :param header: str
        :return: None
        """
        col_array = np.asarray(column)
        col_array = col_array.reshape((self.get_size(),1))
        self.data = np.concatenate((self.data, col_array), axis=1)
        if not header is None:
            self.set_header(header, self.get_dimension() - 1)

    def extract_column(self, column_header):
        """
        Returns the column specified by the parameter as a 1d array
        and a deep copy of the data without the specified column
        :param column_header: str
        :return: (numpy.array, numpy.matrix)
        """
        column_num = self.get_column_number(column_header)
        ret_column = np.matrix(self.data[:,column_num]).getA1()
        ret_matrix = np.matrix(np.zeros((self.get_size(), self.get_dimension() - 1)))
        for i in range(0,ret_matrix.shape[1]):
            if i < column_num:
                ret_matrix[:,i] = self.data[:,i]
            else:
                ret_matrix[:, i] = self.data[:, i + 1]

        return ret_column, ret_matrix


    def has_header(self, header):
        """
        Returns True if the specified header exists in the data set
        :param header: str
        :return: bool
        """
        return header in self.headers

    def set_header(self, new_header, col_num):
        """
        Sets the header for the specified coulmn
        :param new_header: str
        :param col_num: int
        :return: None
        """
        if new_header in self.headers:
            raise KeyError("The header '{}' is already used in this data set".format(new_header))
        old_header = self.get_header(col_num)
        self.headers.pop(old_header, None)
        self.headers[new_header] = col_num

    def get_min(self):
        min = np.linalg.norm(self.data[0,:])
        index = 0
        for i in range(1,self.data.shape[0]):
            norm = np.linalg.norm(self.data[i,:])
            if norm < min:
                min = norm
                index = i
        return index

    def get_max(self):
        max = np.linalg.norm(self.data[0, :])
        index = 0
        for i in range(1, self.data.shape[0]):
            norm = np.linalg.norm(self.data[i, :])
            if norm > max:
                max = norm
                index = i
        return index
