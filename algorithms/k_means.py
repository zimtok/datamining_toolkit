import numpy as np
import random
from algorithms.abstract_algorithm import AbstractAlgorithm
from algorithms.algorithm_exception import AlgorithmException

class KMeansClustering(AbstractAlgorithm):
    """
    Implementation of the k-means clustering algorithm
    """


    def __init__(self):
        """
        Initialise setters and parameters
        """
        AbstractAlgorithm.__init__(self)
        self.setters = {"Number of clusters": self.set_clusters}
        self.result = None
        self.row_to_cluster = None
        self.row_count = 0
        self.final_means = None
        self.clusters = 1

    def create_clusters(self, clusters):
        """
        Create the specified number of clusters from the data
        :param clusters: int
        :return: None
        """
        means = self.init_means(clusters)
        old_means = None

        while not np.array_equal(means, old_means):
            self.find_closest_mean(means)

            old_means = means

            means = self.compute_means(means)
        self.result = self.row_to_cluster
        self.final_means = means

    def init_means(self, clusters):
        """
        Create clusters number of initial centroid selected randomly from data points
        :param clusters: int
        :return: numpy.matrix
        """
        self.row_to_cluster = np.zeros(self.row_count, dtype=int)
        means = np.matrix(np.zeros((clusters, self.data.shape[1])))
        initial_mean_indexes = set()
        while len(initial_mean_indexes) < clusters:
            initial_mean_indexes.add(random.randint(0,self.row_count - 1))

        for i in range(0, clusters):
            means[i, :] = self.data[initial_mean_indexes.pop(), :]
        return means

    def find_closest_mean(self, means):
        """
        Find the colsest centroid for all data points
        :param means: numpy.matrix
        :return: None
        """
        for i in range(0, self.row_count):
            current_row = self.data[i, :]
            min_dist = np.linalg.norm((current_row - means[0, :]), ord=2) ** 2
            cluster_index = 0
            for j in range(1, means.shape[0]):
                dist = np.linalg.norm((current_row - means[j, :]), ord=2) ** 2
                if dist < min_dist:
                    min_dist = dist
                    cluster_index = j
            self.row_to_cluster[i] = cluster_index

    def compute_means(self, old_means):
        """
        Upadte centroids using new cluster members
        :param old_means: numpy.matrix
        :return: numpy.matrix
        """
        new_means = np.zeros_like(old_means)
        #use a vector to keep track of the number of points in each cluster
        cluster_count = np.zeros(old_means.shape[0])
        for i in range(0, self.data.shape[0]):
            cluster_index = self.row_to_cluster[i]
            new_means[cluster_index, :] += self.data[i, :]
            cluster_count[cluster_index] += 1

        for i in range(0, new_means.shape[0]):
            new_means[i, :] = new_means[i, :] / cluster_count[i]

        return new_means

    def get_result(self):
        """
        Getter for the result array
        :return: numpy.array
        """
        return self.result

    def set_clusters(self, clusters):
        """
        Setter for the clusters parameter
        :param clusters: float
        :return: None
        """
        self.clusters = int(clusters)


    def set_data(self, data):
        """
        Setter for the data matrix
        :param data: numpy.matrix
        :return: None
        """
        self.data = np.matrix(data)
        self.row_count = self.data.shape[0]

    def run(self):
        """
        Run the algorithm with the parameters given earlier
        The cluster values are stored in the self.result array
        :return:
        """
        if self.clusters > self.data.shape[0]:
            raise AlgorithmException("The number of clusters must be less than the number of data points")
        if self.clusters < 1:
            raise AlgorithmException("The number of clusters must be at least one")
        self.create_clusters(self.clusters)
        self.row_count = self.data.shape[0]