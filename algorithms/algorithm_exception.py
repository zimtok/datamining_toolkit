
class AlgorithmException(Exception):
    """
    Generic exception class for all algorithms
    """
    def __init__(self, message):
        self.message = message