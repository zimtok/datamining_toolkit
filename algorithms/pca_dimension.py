import algorithms.principal_component as pca
from algorithms.algorithm_exception import AlgorithmException

class PCADimension(pca.PrincipalComponentAnalysis):
    """
    PCA with number of dimensions as parameter
    """
    def __init__(self):
        """
        Initialise setter dictionary and parameters
        """
        pca.PrincipalComponentAnalysis.__init__(self)
        self.setters = {"Dimensions to retain": self.set_dimension}
        self.dimensions = 2


    def set_dimension(self, dimension):
        """
        Set the number of dimensions
        :param dimension: float
        :return: None
        """
        if dimension > self.data.shape[1] or dimension < 1:
            raise AlgorithmException("The target dimension number must be between the dimension of the data set and 1")
        self.dimensions = int(dimension)

    def run(self):
        """
        Run PCA with the specified number of dimensions
        :return: None
        """

        self.reduce_to_dimension(self.dimensions)






