import numpy as np
import algorithms.abstract_algorithm as alg
from algorithms.algorithm_exception import AlgorithmException


class KNearestNeighbours(alg.AbstractAlgorithm):
    """
    Implementation of the k-Nearest Neighbours classification algorithm
    """

    def __init__(self):
        alg.AbstractAlgorithm.__init__(self)
        self.setters = {"Number of neighbours": self.set_neighbours}
        self.data = None
        #classes[i] is the class the i-th row of the data belongs to
        self.classes = None
        self.neighbours = 1



    def get_distance(self, row1, row2):
        """Gets the Euclidean distance of row1 and row2"""
        return np.linalg.norm(row1 - row2, ord=2)**2

    def predict(self, row):
        if row.shape[1] != self.data.shape[1]:
            raise AlgorithmException("The data that is predicted on must have the same dimensions as the training data")
        return self.find_neighbours(row, self.neighbours)

    def find_neighbours(self, row, neighbours):
        """Find the k nearest neighbours of a vector"""
        #We maintain a dict a with the closest neighbours we found. It maps distances to row indices
        current_neighbours = dict()
        closest_classes = list()
        #Initialize the dict with the first k rows of the data matrix
        for i in range(0, neighbours):
            dist = self.get_distance(row, self.data[i, :])
            current_neighbours[dist] = i
        #Keep track of the neighbour furthest from the point
        max_distance = max(current_neighbours.keys())
        for i in range(neighbours, self.data.shape[0]):
            dist = self.get_distance(row, self.data[i, :])
            if dist < max_distance:
                del current_neighbours[max_distance]
                current_neighbours[dist] = i
                max_distance = max(current_neighbours.keys())

        for key in current_neighbours:
            row_index = current_neighbours[key]
            closest_classes.append(self.classes[row_index])

        return max(sorted(set(closest_classes)), key=closest_classes.count)

    def set_classes(self, classes):
        """
        Set the array containing the class values
        :param classes: np.array
        :return: None
        """
        self.classes = classes

    def set_neighbours(self, neighbours):
        """
        Set the number of neighbours to use
        :param neighbours: float
        :return: None
        """
        if neighbours > self.data.shape[0] or neighbours < 1:
            raise AlgorithmException("The number of neighbour must be between one and the number of data points.")
        self.neighbours = int(neighbours)

    def train(self):
        """
        Train the model on the current data
        :return: None
        """

    def set_data(self, data):
        if data.shape[1] == 0:
            raise AlgorithmException("The data matrix is empty.")
        self.data = data

