import numpy as np
import csv


class DescriptiveStatistics:
    """Class for getting basic descriptive data of a DataSet instance"""

    def __init__(self, dataset):
        """
        Initialise data set
        :param dataset: DataSet
        """
        self.dataset = dataset
        self.row_count = self.dataset.get_size()

    def min(self, label):
        """
        Get the minimum of the specified column
        :param label: str
        :return: float
        """
        column = self.dataset.get_column(label)
        return np.amin(column)

    def max(self, label):
        """
        Get the maximum of the specified column
        :param label: str
        :return: float
        """
        column = self.dataset.get_column(label)
        return np.amax(column)

    def mean(self, label):
        """
        Get the mean of the specified column
        :param label: str
        :return: float
        """
        sum = 0.0
        column = self.dataset.get_column(label)
        for i in range(0, self.row_count):
            sum += column[i, 0]
        return sum / self.row_count

    def median(self, label):
        """
        Get the median of the specified column
        :param label: str
        :return: float
        """
        column = self.dataset.get_column(label)
        sorted_values = np.sort(column.getA1())
        return (sorted_values[self.row_count // 2] + sorted_values[(self.row_count - 1) // 2]) / 2

    def std_dev(self, label):
        """
        Get the standard deviation of the specified column
        :param label: str
        :return: float
        """
        if self.row_count == 1:
            return 0
        column = self.dataset.get_column(label)
        mean = self.mean(label)
        sum_squared_diff = 0
        for i in range(0, self.row_count):
            sq_diff = (column[i, 0] - mean) ** 2
            sum_squared_diff += sq_diff
        return np.sqrt(1 / (self.row_count - 1) * sum_squared_diff)

    def write_to_file(self, filename):
        """
        Write statistics of all columns to CSV file specified by filename
        :param filename: str
        :return: None
        """
        with open(filename, "w") as outfile:
            writer = csv.writer(outfile)
            writer.writerow(self.get_header())
            for row in self.get_all_stat():
                writer.writerow(row)


    def get_header(self):
        """
        Get column headers to writing to file
        :return: list
        """
        return ["label", "min", "max", "mean", "median", "std_dev"]

    def get_all_stat(self, rounding = None):
        """
        Generator to iterate over list of statistics for each column (rounded to rounding digits)
        :param rounding: int
        :return: GeneratorType
        """
        for label in self.dataset.get_column_labels():
            row = [label]
            custom_round = lambda x: x
            if not rounding is None:
                custom_round = lambda x: round(x, rounding)
            row.append(custom_round(self.min(label)))
            row.append(custom_round(self.max(label)))
            row.append(custom_round(self.mean(label)))
            row.append(custom_round(self.median(label)))
            row.append(custom_round(self.std_dev(label)))
            yield row




