import numpy as np
import algorithms.abstract_algorithm as alg
from algorithms.algorithm_exception import AlgorithmException

class LinearRegression(alg.AbstractAlgorithm):
    """
    Implementation for linear regression
    """
    def __init__(self):
        alg.AbstractAlgorithm.__init__(self)
        self.dependent = None
        self.explanatory = None
        self.hypothesis = None

    def set_dependent(self, dependent):
        """
        Set the matrix containing values for the dependent variable
        :param dependent: numpy.matrix
        :return: None
        """
        self.dependent = dependent

    def set_explanatory(self, explanatory):
        """
        Set the matrix containing the values for the explanatory variables
        :param explanatory: numpy.matrix
        :return: None
        """
        if explanatory.shape[1] == 0:
            raise AlgorithmException("There are no explanatory variables")
        self.explanatory = np.matrix(np.ones((explanatory.shape[0], explanatory.shape[1] + 1)))
        self.explanatory[:, 1:] = explanatory


    def calculate_hypothesis(self):
        """
        Find the hypothesis vector
        :return: numpy.matrix
        """
        exp = self.explanatory
        return np.linalg.pinv(exp.transpose() * exp) * exp.transpose() * self.dependent

    def run(self):
        """
        Run the model
        :return: None
        """
        self.hypothesis = self.calculate_hypothesis()

    def predict(self, data_point):
        """
        Predict the value of the dependent variable for a given point
        :param data_point: numpy.matrix
        :return: float
        """
        data_vector = np.matrix(np.ones((1, data_point.shape[1] + 1)))
        data_vector[0, 1:] = data_point[0, :]
        if data_vector.shape[0] != self.hypothesis.shape[1] or data_vector.shape[1] != self.hypothesis.shape[0]:
            raise AlgorithmException("The data to predict on must have the same number of dimensions as the explanatory variables")
        return  data_vector * self.hypothesis

    def get_point_data(self):
        """
        Create a matrix of all the dependent and explanatory values
        :return: numpy.matrix
        """
        return np.concatenate((self.explanatory[:, 1:], self.dependent), axis=1)







