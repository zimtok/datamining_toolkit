import numpy as np
from algorithms.abstract_algorithm import AbstractAlgorithm
from algorithms.algorithm_exception import AlgorithmException


class PrincipalComponentAnalysis(AbstractAlgorithm):

    def __init__(self):
        """Constructor for the class"""
        AbstractAlgorithm.__init__(self)
        self.result = None
        self.singular_values = None
        self.eigenvectors = None

    def normalize(self):
        """Perform mean normalization for each column of the data matrix"""
        for j in range(0, self.data.shape[1]):
            mean = np.mean(self.data[:, j])
            for i in range(0, self.data.shape[0]):
                self.data[i, j] -= mean

    def feature_scaling(self):
        """Scale each feature vector so that they have comparable ranges"""
        for j in range(0, self.data.shape[1]):
            std_dev = np.std(self.data[:, j])
            if std_dev == 0:
                continue
            for i in range(0, self.data.shape[0]):
                self.data[i, j] /= std_dev

    def reduce_to_dimension(self, output_dimension):
        """Create a matrix from the data with the specified number of dimensions"""
        u, s = self.get_svd()
        reduced_matrix = u[:, 0:output_dimension]
        self.result = self.data * reduced_matrix

    def get_retained_variance(self, dimensions):
        """
        Get the amount of variance retained with reducing to a given number of dimesnions
        :param dimensions: int
        :return: float
        """
        u, s = self.get_svd()
        retained = 0
        total = 0
        for i in range(0, dimensions):
            retained += s[i]
        for i in range(0, s.size):
            total += s[i]
        return retained/total


    def reduce_to_variance(self, variance_to_retain=0.9):
        """
        Reduce data to a specified fraction of variance [0, 1]
        :param variance_to_retain: float
        :return: None
        """
        dimensions = 1
        while self.get_retained_variance(dimensions) < variance_to_retain:
            dimensions += 1
        self.reduce_to_dimension(dimensions)

    def get_svd(self):
        """
        Create a variance matrix and get its SVD decomposition
        :return:
        """
        if self.singular_values is None and self.eigenvectors is None:
            observations = self.data.shape[0]
            variance_matrix = (1 / observations) * self.data.transpose() * self.data
            try:
                u, s, v = np.linalg.svd(variance_matrix)
            except np.linalg.LinAlgError:
                raise AlgorithmException("The SVD-decomposition of the matrix cannot be computed")
            self.eigenvectors = u
            self.singular_values = s
        return self.eigenvectors, self.singular_values

    def set_data(self, data):
        """
        Set,  normalize and scale the data to work on
        :param data: numpy.matrix
        :return: None
        """
        self.data = np.matrix(data)
        self.normalize()
        self.feature_scaling()
        self.eigenvectors = None
        self.singular_values = None