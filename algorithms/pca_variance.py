import algorithms.principal_component as pca
from algorithms.algorithm_exception import AlgorithmException

class PCAVariance(pca.PrincipalComponentAnalysis):
    """
    PCA with percentage of variance as parameter
    """
    def __init__(self):
        """
        Initialise setter dictionary and parameters
        """
        pca.PrincipalComponentAnalysis.__init__(self)
        self.setters = {"Ratio of variance to retain": self.set_variance}
        self.variance = 0.0

    def set_variance(self, variance):
        """
        Set the percentage of variance to retain
        :param dimension: float
        :return: None
        """
        if variance < 0 or  variance > 1:
            raise AlgorithmException("The retained variance must be between 0.0 and 1.0")
        self.variance = variance

    def run(self):
        """
        Run PCA with the specified percentage of variance
        :return:
        """
        self.reduce_to_variance(self.variance)