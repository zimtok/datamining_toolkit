import numpy as np
import algorithms.abstract_algorithm as alg
from algorithms.algorithm_exception import AlgorithmException


class DbScan(alg.AbstractAlgorithm):
    """
    Implementation of the DBSCAN clustering algorithm
    """

    def __init__(self):
        """
        Initialise setters and parameters
        """
        alg.AbstractAlgorithm.__init__(self)
        self.epsilon = 1
        self.min_points = 1
        self.setters={"Epsilon": self.set_epsilon, "Minimum points":self.set_min_points}
        self.result = None

    def run(self):
        """
        Run the DBSCAN algorithm with previously set parameters
        The results are stored in the self.result array
        :return: None
        """
        self.result = np.zeros((self.data.shape[0]))
        visited = set()
        cluster_id = 0
        for i in range(0, self.data.shape[0]):
            if not i in visited:
                visited.add(i)
                neighbours = self.get_neighbours(i)
                if len(neighbours) > self.min_points:
                    cluster_id += 1
                    self.expand_cluster(i, neighbours, visited, cluster_id)



    def set_epsilon(self, epsilon):
        """
        Set the epsilon distance parameter
        :param epsilon: float
        :return: None
        """
        if(epsilon < 0):
            raise AlgorithmException("The epsilon distance must be non-negative")
        self.epsilon = epsilon

    def set_min_points(self, min_points):
        """
        Set the minimal number of neighbouring points
        :param min_points: float
        :return: None
        """
        if (min_points < 0):
            raise AlgorithmException("The number of minimal points must be non-negative")
        self.min_points = int(min_points)

    def get_distance(self, p1, p2):
        """
        Calculate the Euclidean distance pf two points
        :param p1: numpy.matrix
        :param p2: numpy.matrix
        :return: float
        """
        return np.linalg.norm(p1 - p2, ord=2) ** 2

    def get_neighbours(self, point_index):
        """
        Return a list of points that are within epsilon distance of the point specified by the parameter
        :param point_index: int
        :return: list
        """
        neighbour_list = []
        for i in range(0,self.data.shape[0]):
            if self.get_distance(self.data[point_index], self.data[i, :]) < self.epsilon:
                neighbour_list.append(i)
        return neighbour_list

    def expand_cluster(self, point_index, neighbours, visited, cluster_id):
        """
        Expand a cluster by adding appropriate neighbouring points to it
        :param point_index: int
        :param neighbours: list
        :param visited: set
        :param cluster_id: int
        :return: None
        """
        self.result[point_index] = cluster_id
        i = 0
        while i < len(neighbours):
            neighbour_index = neighbours[i]
            if not neighbour_index in visited:
                visited.add(neighbour_index)
                new_neighbours = self.get_neighbours(neighbour_index)
                if len(new_neighbours) >= self.min_points:
                    neighbours.extend(new_neighbours)
            if self.result[neighbour_index] == 0:
                self.result[neighbour_index] = cluster_id
            i += 1


    def get_result(self):
        """
        Getter for the result array
        :return: numpy.array
        """
        return self.result



