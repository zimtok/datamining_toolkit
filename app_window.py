from tkinter import *
import dataset_view as dsw
import dimension_reduction_view as drw
import clustering_view as cw
import classification_view as cls
import regression_view as rw
import  cumulative_plot_view as cpw
import boxplot_view as bpw

class AppWindow:
    """
    Class to contain the root window of the program
    """

    window_width = 1000
    window_heigth = 500

    def __init__(self):

        """Initialise the menu bar"""
        self.root = Tk()
        self.root.title("Datamining toolkit")

        self.root.resizable(width=False, height=False)
        self.root.geometry('{}x{}'.format(self.window_width, self.window_heigth))
        self.root.columnconfigure(1, weight=1)


        menu_bar = Menu(self.root)
        self.root.configure(menu=menu_bar)
        menu_bar.add_command(label="Open dataset", command=self.open_dataset)
        menu_bar.add_command(label="Dimensionality reduction", command=self.dim_reduction)
        menu_bar.add_command(label="Clustering", command=self.cluster)
        menu_bar.add_command(label="Classification", command=self.classification)
        menu_bar.add_command(label="Regression", command=self.regression)
        plot_menu = Menu(self.root)
        plot_menu.add_command(label="Cumulative fraction", command=self.cumulative_fraction)
        plot_menu.add_command(label="Boxplot", command=self.boxplot)
        menu_bar.add_cascade(menu=plot_menu, label="Plots")
        menu_bar.add_command(label="Quit", command=self.root.quit)

        self.current_view = dsw.DatasetView(self.root, None)

        self.root.mainloop()

    def open_dataset(self):
        """
        Callback for the open_dataset option
        :return: None
        """
        data = None
        if not self.current_view is None:
            self.current_view.destroy()
            data = self.current_view.get_data()
        self.current_view = dsw.DatasetView(self.root, data)

    def dim_reduction(self):
        """
        Callback for the dimensionality reduction option
        :return: None
        """
        data = None
        if not self.current_view is None:
            data = self.current_view.get_data()
            self.current_view.destroy()
        self.current_view = drw.DimensionReductionView(self.root, data)

    def cluster(self):
        """
        Callback for the clustering option
        :return: None
        """
        data = None
        if not self.current_view is None:
            data = self.current_view.get_data()
            self.current_view.destroy()
        self.current_view = cw.ClusteringView(self.root, data)

    def classification(self):
        """
        Callback for the classification option
        :return: None
        """
        data = None
        if not self.current_view is None:
            data = self.current_view.get_data()
            self.current_view.destroy()
        self.current_view = cls.ClassificationView(self.root, data)

    def regression(self):
        """
        Callback for the regression option
        :return: None
        """
        data = None
        if not self.current_view is None:
            data = self.current_view.get_data()
            self.current_view.destroy()
        self.current_view = rw.RegressionView(self.root, data)

    def cumulative_fraction(self):
        """
        Callback for the plots -> cumulative fraction option
        :return: None
        """
        data = None
        if not self.current_view is None:
            data = self.current_view.get_data()
            self.current_view.destroy()
        self.current_view = cpw.CumulativePlotView(self.root, data)


    def boxplot(self):
        """
        Callback for the plots -> boxplot option
        :return: None
        """
        data = None
        if not self.current_view is None:
            data = self.current_view.get_data()
            self.current_view.destroy()
        self.current_view = bpw.BoxPlotView(self.root, data)