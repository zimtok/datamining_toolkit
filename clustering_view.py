from tkinter import messagebox
from tkinter import ttk

import algorithm_view as aw
import algorithms.dbscan as dbscan
import algorithms.k_means as kmc
import visualisation.scatterplot as sp
from algorithms.algorithm_exception import AlgorithmException


class ClusteringView(aw.AlgorithmView):
    """
    View for clustering algorithms
    """
    methods = {"k-means": kmc.KMeansClustering, "DBSCAN": dbscan.DbScan}

    def __init__(self, root, dataset):
        self.clustering_alg = None
        aw.AlgorithmView.__init__(self, root, dataset)


    def create_option_frame(self):
        """
        Created method selector and 'Go' button
        :return: None
        """
        self.add_method_selector()
        ttk.Button(self.option_frame, text='Go', command=self.run_method).grid(row=self.row_index, column=0)
        self.row_index += 1

    def run_method(self):
        """
        Run the selected algprithm
        :return: None
        """
        if len(self.method.get()) > 0:
            try:
                selected_method = self.methods[self.method.get()]
                self.clustering_alg = selected_method()
                self.clustering_alg.set_data(self.dataset.get_data_matrix())
                self.clustering_alg.set_parameters(self.create_parameter_dict())
                self.clustering_alg.run()
                self.add_result_buttons()
            except AlgorithmException as exception:
                messagebox.showerror(title="Error", message=exception.message)




    def update_plot(self):
        """
        Create a plot with the results
        :return: None
        """
        scatter_plot = sp.ScatterPlot(self.dataset.get_data_matrix(), groups=self.clustering_alg.get_result())
        scatter_plot.create_plot()
        self.figure = scatter_plot.get_figure()


    def get_data(self):
        """
        Return the data with the cluster IDs appended
        :return: algorithm.DataSet
        """
        if not self.dataset is None and not self.clustering_alg is None:
            ret = self.dataset.clone()
            label_postfix = 1
            class_label = "generated_class_{}"
            while(ret.has_header(class_label.format(label_postfix))):
                label_postfix += 1
            ret.append_column(self.clustering_alg.get_result(), class_label.format(label_postfix))
            return ret
        else:
            return self.dataset







