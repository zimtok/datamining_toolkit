from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from tkinter import ttk

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg


class AlgorithmView:
    '''
    Base class for the GUI views running an algorithm
    '''
    methods = dict()
    def __init__(self, root, dataset):
        """
        Set the root frame and the dataset to work on
        :param root: ttk.Frame
        :param dataset: algorithms.DataSet
        """
        self.root = root
        self.dataset = dataset
        self.option_frame = ttk.Frame(self.root)
        self.option_frame.grid(row=0, column=0)
        self.data_frame = ttk.Frame(self.root)
        self.data_frame.grid(row=0, column=1)
        self.row_index = 0
        self.method = StringVar()
        if self.check_dataset():
            self.create_option_frame()
            ttk.Label(self.data_frame, text="Results will be displayed here", font=("sans-serif", 14)).grid(row=0,
                                                                                                            column=0)
        for child in self.option_frame.winfo_children():
            child.grid_configure(padx=10, pady=10)
        self.parameter_values = dict()
        self.method_widgets = list()
        self.result_widgets = list()
        self.figure = None


    def create_option_frame(self):
        """
        Create the frame containing options for the algorithm
        :return:
        """
        pass


    def add_method_selector(self):
        """
        Put the available algorithms to a ComboBox
        :return: None
        """
        ttk.Label(self.option_frame, text="Method to use").grid(row=self.row_index, column=0)
        method_menu = ttk.Combobox(self.option_frame, state="readonly", values=list(self.methods.keys()), textvariable=self.method)
        method_menu.grid(row=self.row_index, column=1)
        self.row_index += 1
        method_menu.bind('<<ComboboxSelected>>', self.display_parameters)

    def destroy(self):
        """
        Remove all frames
        :return: None
        """
        self.option_frame.destroy()
        self.data_frame.destroy()

    def check_dataset(self):
        """
        Show an error meassage if no dataset is loaded
        :return: None
        """
        if self.dataset is None:
            messagebox.showerror(title="Error", message="You haven't loaded any data")
            return False
        else:
            return True

    def display_parameters(self, *args):
        """
        Display parameters for the seleceted algorithm and provide text fields to set their values
        :param args:
        :return: None
        """
        #First clear everything used by the previous algorithm
        self.clear_parameters()
        self.clear_result_buttons()
        selected_method = self.method.get()
        if len(selected_method) > 0:
            algorithm = self.methods[selected_method]()
            parameters = algorithm.get_parameters()
            for param in parameters:
                self.parameter_values[param] = DoubleVar()
                label = ttk.Label(self.option_frame, text=param, justify=LEFT)
                label.grid(row=self.row_index, column=0, sticky=(W, N))
                entry = ttk.Entry(self.option_frame, textvariable=self.parameter_values[param])
                entry.grid(row=self.row_index, column=1, sticky=(E, N))
                self.method_widgets.append((label, entry))
                self.row_index += 1
            for child in self.option_frame.winfo_children():
                child.grid_configure(padx=10, pady=10)

    def create_parameter_dict(self):
        """
        Build a dictionary from parameter identifiers and textbox values
        :return: dict
        """
        return {param: param_var.get() for param, param_var in self.parameter_values.items()}

    def generate_unique_label(self, dataset, label_template):
        """
        Generate a unique label to append to a data set
        :param dataset: algorithms.DataSet
        :param label_template: str
        :return: str
        """
        label_number = 1
        while dataset.has_header(label_template.format(label_number)):
            label_number += 1
        return label_template.format(label_number)

    def add_result_buttons(self):
        """
        Add buttons to view/save the result data and plot
        :return: None
        """
        for child in self.data_frame.winfo_children():
            child.destroy()
        self.clear_result_buttons()
        self.add_button("Show results (sample)", self.display_data)
        self.add_button("Save results", self.save_data)
        self.add_button("Show plot", self.display_plot)
        self.add_button("Save plot", self.save_plot)
        for child in self.option_frame.winfo_children():
            child.grid_configure(padx=10, pady=10)

    def add_button(self, text, callback):
        """
        Add a button to the optiuon_frame
        :param text: str
        :param callback: function
        :return: None
        """
        button = ttk.Button(self.option_frame, text=text, command=callback)
        button.grid(row=self.row_index, column=0)
        self.row_index += 1
        self.result_widgets.append(button)


    def display_data(self):
        """
        Display all result data on the data_frame
        :return: None
        """
        for child in self.data_frame.winfo_children():
            child.destroy()
        data = self.get_data()
        for i in range(0,data.get_dimension()):
            header = data.get_header(i)
            ttk.Label(self.data_frame, text=header).grid(row=0, column = i)
        for i in range(0, 25):
            if i == data.get_size():
                break
            row = data.get_row(i)
            for j in range(0, data.get_dimension()):
                ttk.Label(self.data_frame, text=row[0, j]).grid(row=i + 1, column=j)

    def display_plot(self):
        """
        Display a plot on the data_frame
        :return:
        """
        for child in self.data_frame.winfo_children():
            child.destroy()
        self.update_plot()
        if not self.figure is None:
            canvas = FigureCanvasTkAgg(self.figure, master=self.data_frame)
            canvas.get_tk_widget().grid(row=0, column=0)
            canvas.draw()
        else:
            ttk.Label(self.data_frame, text="No plot to show\nThe dimension of the data is too high", font=("sans-serif", 14)).grid(row=0, column=0)


    def save_plot(self):
        """
        Save plot to image file
        :return: None
        """
        outfile_name = filedialog.asksaveasfilename()
        if outfile_name != '':
            self.figure.savefig(outfile_name)


    def save_data(self):
        """
        Save data to CSV file
        :return: None
        """
        outfile_name = filedialog.asksaveasfilename()
        if outfile_name != '':
            save_data = self.get_data()
            save_data.write_to_file(outfile_name)

    def get_data(self):
        """
        Get result data
        :return: algorithms.DataSet
        """
        return self.dataset

    def clear_parameters(self):
        """
        Clear all parameter setter textboxes from the option_frame
        :return: None
        """
        for widget_pair in self.method_widgets:
            self.row_index -= 1
            widget_pair[0].destroy()
            widget_pair[1].destroy()
        self.parameter_values.clear()
        self.method_widgets.clear()

    def clear_result_buttons(self):
        """
        Clear all result buttons from the option_frame
        :return: None
        """
        for widget in self.result_widgets:
            self.row_index -= 1
            widget.destroy()
        self.result_widgets.clear()

    def update_plot(self):
        """
        Create a plot (implemented in subclasses)
        """
        pass

    def run_method(self):
        pass

