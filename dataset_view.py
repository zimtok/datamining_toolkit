from tkinter import  *
from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox
import algorithms.dataset as ds
from algorithms.algorithm_exception import AlgorithmException
import algorithms.descriptive_stat as stat

class DatasetView:
    """View for loading new files and basic statistics"""
    def __init__(self, root, data):
        self.root = root
        self.option_frame = self.create_option_frame()
        self.dataset = None
        self.data_frame = ttk.Frame(self.root)
        self.data_frame.grid(row=0, column=1, rowspan=2)
        self.root.columnconfigure(1, weight=1)
        self.statistics = None
        if not data is None:
            self.add_old_data(data)
        else:
            ttk.Label(self.data_frame, text="Load a data file to start working", font=("sans-serif", 14)).grid(row=0, column=0)



    def create_option_frame(self):
        """
        Set up option fram with load button
        :return: None
        """
        option_frame = ttk.Frame(self.root)
        option_frame.grid(row=0, column=0)
        ttk.Label(option_frame, text="Open CSV file", font=("sans-serif", 14)).grid(row=0, column=0)
        ttk.Button(option_frame, text="Browse", command = self.create_dataset).grid(row=1, column=0)
        for child in option_frame.winfo_children():
            child.grid_configure(padx=10, pady=10)

        return option_frame

    def create_dataset(self):
        """
        Load data from file
        :return: None
        """
        filename = filedialog.askopenfilename()
        if len(filename) > 0:
            self.dataset = ds.DataSet()
            try:
                self.dataset.read_from_file(filename)
            except AlgorithmException as e:
                messagebox.showerror(title="Error", message=e.message)
                return

            self.update_data_frame()
            self.add_save_button()

    def update_data_frame(self):
        """
        Display descriptive statistics
        :return: None
        """
        for child in self.data_frame.winfo_children():
            child.destroy()
        descr_stat = stat.DescriptiveStatistics(self.dataset)
        col_index = 0
        for header in descr_stat.get_header():
            ttk.Label(self.data_frame, text=header, font=("sans-serif", 14), anchor=W).grid(row=0, column=col_index)
            col_index += 1
        row_index = 1
        try:
            for row in descr_stat.get_all_stat(rounding=2):
                col_index = 0
                for cell in row:
                    ttk.Label(self.data_frame, text=cell, font=("sans-serif", 14), anchor=W, justify=RIGHT).grid(row=row_index, column=col_index)
                    col_index += 1
                row_index += 1
            self.statistics = descr_stat
        except KeyError:
            messagebox.showerror(title="Error", message="There are extra labels in the data file")
            for child in self.data_frame.winfo_children():
                child.destroy()

        for child in self.data_frame.winfo_children():
            child.grid_configure(padx=10, pady=10)


    def add_save_button(self):
        """
        Enable saving to CSV
        :return: None
        """
        ttk.Label(self.option_frame, text="Save statistics", font=("sans-serif", 14)).grid(row=2, column=0)
        ttk.Button(self.option_frame, text="Save", command=self.save).grid(row=3, column=0)
        for child in self.option_frame.winfo_children():
            child.grid_configure(padx=10, pady=10)

    def get_data(self):
        """
        Return the loaded data
        :return: algorithms.DataSet
        """
        return self.dataset

    def save(self):
        """Save to CSV"""
        filename = filedialog.asksaveasfilename()
        if len(filename) > 0:
            self.statistics.write_to_file(filename)


    def destroy(self):
        """
        Clear the the option_frame and data_frame
        :return: None
        """
        self.option_frame.grid_remove()
        self.data_frame.grid_remove()

    def add_old_data(self, data):
        self.dataset = data
        self.update_data_frame()




