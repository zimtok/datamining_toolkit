from tkinter import *
from tkinter import messagebox
from tkinter import ttk

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

import algorithm_view as aw
import algorithms.linear_regression as lr
import visualisation.regression_plot as rp
from algorithms.algorithm_exception import AlgorithmException


class RegressionView(aw.AlgorithmView):
    """
    View for regression algorithms
    """
    methods = {"Linear regression": lr.LinearRegression}

    def __init__(self, root, dataset):
        self.dependent_label = StringVar()
        self.regression_alg = None

        aw.AlgorithmView.__init__(self, root, dataset)

    def create_option_frame(self):
        """
        Create the option frame
        :return: None
        """
        self.add_method_selector()
        ttk.Label(self.option_frame, text="Column containing the dependent variable").grid(row=1, column=self.row_index)
        self.row_index += 1
        ttk.Combobox(self.option_frame, state="readonly", values=[x for x in self.dataset.get_column_labels()],
                     textvariable=self.dependent_label).grid(row=self.row_index, column=1)
        self.row_index += 1
        ttk.Button(self.option_frame, text="Go", command=self.run_method).grid(row=self.row_index, column=0)
        self.row_index += 1

    def run_method(self):
        """
        Run the selected algorithm
        :return: None
        """

        if len(self.method.get()) > 0 and len(self.dependent_label.get()) > 0:
            try:
                selected_method = self.methods[self.method.get()]
                dependent_column = self.dataset.get_column(self.dependent_label.get())
                col, explanatory_matrix = self.dataset.extract_column(self.dependent_label.get())
                self.regression_alg = selected_method()
                self.regression_alg.set_dependent(dependent_column)
                self.regression_alg.set_explanatory(explanatory_matrix)
                self.regression_alg.run()
                self.add_result_buttons()
            except AlgorithmException as exception:
                messagebox.showerror(title="Error", message=exception.message)

    def update_plot(self):
        """
        Create a scatterplot with a regression line
        :return:
        """
        try:
            regression_plot = rp.RegressionPlot(self.dataset.get_data_matrix(), self.regression_alg)
            regression_plot.create_plot(self.dataset.get_min(), self.dataset.get_max())
            self.figure = regression_plot.get_figure()
            canvas = FigureCanvasTkAgg(self.figure, master=self.data_frame)
            canvas.get_tk_widget().grid(row=0, column=0)
            canvas.draw()
        except AlgorithmException as exception:
            messagebox.showerror(title="Error", message=exception.message)


