from tkinter import *
from tkinter import messagebox
from tkinter import ttk

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

import algorithm_view as aw
import algorithms.dataset as ds
import algorithms.pca_dimension as pcad
import algorithms.pca_variance as pcav
import visualisation.scatterplot as sp
from algorithms.algorithm_exception import AlgorithmException


class DimensionReductionView(aw.AlgorithmView):
    """
    Viev for dimension reduction algorithms
    """
    methods = {"PCA (dimension)": pcad.PCADimension, "PCA (variance)": pcav.PCAVariance}

    def __init__(self, root, dataset):
        self.to_variance = BooleanVar()
        self.dimensions = IntVar()
        self.variance_percent = DoubleVar()
        self.reduction_algorithm = None
        aw.AlgorithmView.__init__(self, root,dataset)



    def create_option_frame(self):
        """Creation option_frame with method selection"""
        self.add_method_selector()
        ttk.Button(self.option_frame, text='Go', command=self.run_method).grid(row=self.row_index, column=0)
        self.row_index += 1


    def run_method(self):
        """
        Run the selected method
        :return: None
        """

        if len(self.method.get()) > 0:
            try:
                selected_method = self.methods[self.method.get()]
                self.reduction_algorithm = selected_method()
                self.reduction_algorithm.set_data(self.dataset.get_data_matrix())
                self.reduction_algorithm.set_parameters(self.create_parameter_dict())
                self.reduction_algorithm.run()

                self.update_plot()

                self.add_result_buttons()
            except AlgorithmException as exception:
                messagebox.showerror(title="Error", message=exception.message)


    def update_plot(self):
        """
        Create a scatterplot from the data
        :return: None
        """
        scatter_plot = sp.ScatterPlot(self.reduction_algorithm.result)
        scatter_plot.create_plot()
        self.figure = scatter_plot.get_figure()
        canvas = FigureCanvasTkAgg(self.figure, master=self.data_frame)
        canvas.get_tk_widget().grid(row=0, column=0)
        canvas.draw()

    def get_data(self):
        """
        return a data set with reduced dimensions
        :return: algorithms.DataSet
        """
        if not self.reduction_algorithm is None:
            ret = ds.DataSet()
            ret.create_from_matrix(self.reduction_algorithm.result)
            for i in range(0, ret.get_dimension()):
                ret.set_header("dim_red_{}".format(i), i)
            return ret
        else:
            return self.dataset

