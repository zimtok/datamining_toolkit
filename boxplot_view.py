from plot_view import PlotView
from visualisation.boxplot import BoxPlot

class BoxPlotView(PlotView):

    def __init__(self, root, dataset):
        PlotView.__init__(self, root, dataset)

    def get_figure(self, columns):
        fig = BoxPlot(columns)
        fig.create_plot()
        return fig.get_figure()