from plot_view import PlotView
from visualisation.cumulative_fraction import CumulativeFraction

class CumulativePlotView(PlotView):

    def __init__(self, root, dataset):
        PlotView.__init__(self, root, dataset)

    def get_figure(self, columns):
        fig = CumulativeFraction(columns)
        fig.create_plot()
        return fig.get_figure()